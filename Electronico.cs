﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ejemplo_Interfaz
{
    public class Electronico
    {
        Boolean _apag;


        public Electronico(Boolean apagado) {
            _apag = apagado;
        }

        public void Apagar() { 

        }
        public Boolean Apagado
        {
            get { return _apag; }
            set { _apag = value; }
        }
    }
}
