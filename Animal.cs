﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ejemplo_Interfaz
{
    class Animal:ISerVivo
    {
        String _cont = "Ninguno";
        public Boolean ConPelo { get; set; }
        public int Patas { get; set; }
        public Boolean EstaVivo()
        {
            return true;
        }
        public String Continente { get { return _cont; } set { _cont = value; } }
    }
}
