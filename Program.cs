﻿using System;
using System.Collections;
using System.Linq;
using System.Text;

namespace Ejemplo_Interfaz
{
    class Program
    {
        static void Main(string[] args)
        {
            

            Humano h = new Humano();
            ISerVivo ihum = new Humano();
            ISerVivo ian = new Animal();
            h.Edad = 50;
            h.CURP = "AAMJ860705";
            h.Nombre = "Juan";
            Console.WriteLine(h.Edad);
            Console.WriteLine(h.CURP);
            Console.WriteLine(h.Nombre);
            Console.WriteLine(ihum.EstaVivo());

            ISerVivo[] seres = { h, ihum, ian, new Animal(){ Patas = 4, Continente="asdasda"} };

            foreach (ISerVivo i in seres) {
                Console.WriteLine(i.Continente);
            }
            Console.ReadLine();

            Electronico e = new Electronico(true);
            Tele tv = new Tele(false);
            Bocina boc = new Bocina(true);

            Electronico e2 = new Tele(false);

            Electronico[] elecs = {e,tv, boc,e2};
            foreach (Electronico el in elecs)
            {
                Console.WriteLine(el.Apagado);
            }
            Console.ReadLine();
        }
    }
}
